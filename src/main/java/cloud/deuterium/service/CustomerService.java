package cloud.deuterium.service;

import cloud.deuterium.entity.Customer;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;

import static cloud.deuterium.exception.ExceptionMessages.NOT_FOUND;

/**
 * Created by Milan Stojkovic 10-Apr-2021
 */

@ApplicationScoped
public class CustomerService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public List<Customer> getAll(List<String> sortQuery, int pageIndex, int pageSize) {
        log.info("Getting all Customers from DB with query params: pageIndex={} pageSize={} and sortQuery={}",
                pageIndex, pageSize, sortQuery);
        Page page = Page.of(pageIndex, pageSize);
        Sort sort = extractSortFromQuery(sortQuery);
        return Customer.findAll(sort)
                .page(page)
                .list();
    }

    public Customer getById(Long id) {
        log.info("Getting Customer with id={}", id);
        Optional<Customer> optional = Customer.findByIdOptional(id);
        return optional.orElseThrow(() -> new NotFoundException(String.format(NOT_FOUND, id)));
    }

    @Transactional
    public Customer saveNew(Customer customer) {
        log.info("Saving new Customer: {}", customer);
        customer.id = null;
        Customer.persist(customer);
        return customer;
    }

    @Transactional
    public Customer update(Long id, Customer customer) {
        log.info("Updating Customer: {}", customer);
        Optional<Customer> optional = Customer.findByIdOptional(id);
        optional.orElseThrow(() -> new BadRequestException(String.format(NOT_FOUND, id)));
        customer.id = id;
        customer.persist();
        return customer;
    }

    @Transactional
    public boolean deleteById(Long id) {
        log.info("Delete Customer with id={}", id);
        Optional<Customer> optional = Customer.findByIdOptional(id);
        optional.orElseThrow(() -> new BadRequestException(String.format(NOT_FOUND, id)));
        return Customer.deleteById(id);
    }

    private Sort extractSortFromQuery(List<String> sortQueryList) {
        Sort sort = Sort.by();
        for (String sortQuery : sortQueryList) {
            addSort(sort, sortQuery);
        }
        return sort;
    }

    private void addSort(Sort sort, String sortQuery) {
        String[] split = sortQuery.split(",");

        if (split.length == 1) {
            sort.and(split[0].trim());
            return;
        }

        if (split.length == 2) {
            try {
                Sort.Direction direction = Sort.Direction.valueOf(split[1].trim());
                sort.and(split[0].trim(), direction);
                return;
            } catch (IllegalArgumentException e) {
                log.error("Illegal value for Sort Direction: {}", e.getMessage());
                throw new BadRequestException("Illegal value for Sort Direction. It can be only " +
                        Sort.Direction.Ascending.name() + " or " + Sort.Direction.Descending.name() +
                        " but here found: " + split[1]);
            }
        }

        log.error("Query parameter sort is not valid: {}", sortQuery);
        throw new BadRequestException("Query parameter sort is not valid: " + sortQuery);
    }
}

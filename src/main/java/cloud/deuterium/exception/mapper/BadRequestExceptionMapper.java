package cloud.deuterium.exception.mapper;

import cloud.deuterium.exception.ErrorResponse;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by Milan Stojkovic 11-Apr-2021
 */

@Provider
public class BadRequestExceptionMapper implements ExceptionMapper<BadRequestException> {
    @Override
    public Response toResponse(BadRequestException e) {
        return Response
                .status(e.getResponse().getStatus())
                .entity(ErrorResponse.builder()
                        .id(UUID.randomUUID().toString())
                        .message(e.getMessage())
                        .status(e.getResponse().getStatus())
                        .time(LocalDateTime.now())
                        .build())
                .build();
    }
}

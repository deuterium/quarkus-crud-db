package cloud.deuterium.exception;

/**
 * Created by Milan Stojkovic 10-Apr-2021
 */
public class ExceptionMessages {
    public static final String NOT_FOUND = "Customer with id=%s is not found";
}

package cloud.deuterium.exception;

import java.time.LocalDateTime;

/**
 * Created by Milan Stojkovic 13-Apr-2021
 */

public class ErrorResponse {
    public String id;
    public String message;
    public int status;
    public LocalDateTime time;

    public ErrorResponse(String id, String message, int status, LocalDateTime time) {
        this.id = id;
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {
        private String id;
        private String message;
        private int status;
        private LocalDateTime time;

        public ErrorResponse.Builder id(String id){
            this.id = id;
            return this;
        }

        public ErrorResponse.Builder message(String message){
            this.message = message;
            return this;
        }

        public ErrorResponse.Builder status(int status){
            this.status = status;
            return this;
        }

        public ErrorResponse.Builder time(LocalDateTime time){
            this.time = time;
            return this;
        }

        public ErrorResponse build(){
            return new ErrorResponse(this.id, this.message, this.status, this.time);
        }
    }
}

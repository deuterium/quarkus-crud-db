package cloud.deuterium.resource;

import cloud.deuterium.entity.Customer;
import cloud.deuterium.service.CustomerService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/v1/customers")
public class CustomerResource {

    @Inject
    CustomerService customerService;

    @GET
    @Produces(MediaType.APPLICATION_JSON) // ?sort=address,Ascending&sort=name
    public Response getCustomers(@QueryParam("sort") List<String> sortQuery,
                                 @QueryParam("page") @DefaultValue("0") int pageIndex,
                                 @QueryParam("size") @DefaultValue("10") int pageSize) {
        List<Customer> customers = customerService.getAll(sortQuery, pageIndex, pageSize);
        return Response.ok()
                .entity(customers)
                .build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@PathParam("id") Long id) {
        Customer customer = customerService.getById(id);
        return Response.ok()
                .entity(customer)
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveNewCustomer(Customer customer) {
        Customer savedCustomer = customerService.saveNew(customer);
        return Response.ok()
                .entity(savedCustomer)
                .build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCustomer(@PathParam("id") Long id, Customer customer) {
        Customer updatedCustomer = customerService.update(id, customer);
        return Response.ok()
                .entity(updatedCustomer)
                .build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteCustomer(@PathParam("id") Long id) {
        customerService.deleteById(id);
        return Response.noContent().build();
    }


}

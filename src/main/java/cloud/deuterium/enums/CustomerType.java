package cloud.deuterium.enums;

/**
 * Created by Milan Stojkovic 10-Apr-2021
 */

public enum CustomerType {
    HOTEL, TRAVEL_AGENCY, RENTAL_CAR, RESTAURANT
}

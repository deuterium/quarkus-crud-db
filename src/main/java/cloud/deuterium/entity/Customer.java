package cloud.deuterium.entity;

import cloud.deuterium.enums.CustomerType;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Parameters;

import javax.persistence.Entity;
import java.util.List;

/**
 * Created by Milan Stojkovic 10-Apr-2021
 */


@Entity
public class Customer extends PanacheEntity {
    public String name;
    public CustomerType type;
    public String city;
    public String country;
    public String address;
    public String contact;

    public static List<Customer> findByCity(String city) {
        PanacheQuery<Customer> panacheQuery = Customer.find("city", city);
        return panacheQuery.list();
    }

    public static List<Customer> findByCountryAndType(String country, CustomerType type) {
        PanacheQuery<Customer> panacheQuery = Customer.find("country = :c and type = :t",
                Parameters.with("c", country).and("t", type).map());
        return panacheQuery.list();
    }


}
